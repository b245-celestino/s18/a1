// 1
function sum(x, y) {
  console.log("Displayed sum of " + x, "and", +y);
  console.log(x + y);
}

sum(5, 15);

// 2
function difference(x, y) {
  console.log("Displayed difference of " + x, "and", +y);
  console.log(x - y);
}

difference(20, 5);

function showProduct(x, y) {
  console.log("The product of " + x, "and", +y);
  return x * y;
}

let product = showProduct(50, 10);
console.log(product);

// 2

function showQuotient(x, y) {
  console.log("The quotient of " + x, "and", +y);
  return x / y;
}

let quotient = showQuotient(50, 10);
console.log(quotient);

// 3

function area(x) {
  console.log("The result of getting the area of a circle with " + x, "radius");
  const pi = 3.14;
  return pi * x ** 2;
}

let circleArea = area(15);
console.log(circleArea);

// 4

function averageNumbers(w, x, y, z) {
  console.log("The average of " + w, "," + x, "," + y, "," + z);
  return (w + x + y + z) / 4;
}

let averageVar = averageNumbers(20, 40, 60, 80);
console.log(averageVar);

// 5

function checkIf(x, y) {
  console.log("Is " + x, "/", +y, "a passing score");
  isPassed = (x / y) * 100 >= 75;
  return isPassed;
}

let ifPassingScore = checkIf(38, 50);
console.log(ifPassingScore);
